import {
  PixelRatio,
  StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors'
const styles = StyleSheet.create({

  backgroundCoverStyles: {
    width: "100%", height: 300
  },

  subViewStyles: {
    alignContent: 'center',
    alignItems: 'center',
    top: 20
  },

  barcodeButtonStyles: {
    width: 50,
    height: 50,
    borderBottomRightRadius: 20,
    borderTopRightRadius: 20
  },

  barcodeIconContainerStyles: {
    justifyContent: 'center',
    alignItems: 'center',
    top: 10
  },

  subContainerSTyles: {
    flex: 1,
  },

  subTitleContainerStyles: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  likeIconContainerStyles: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    left: -10
  },

  timingContainerStyles: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  ratingSubContainerStyles: {
    flexDirection: 'row', left: 10
  },

  timingSubContainerStyles: {
    flexDirection: 'row', top: 10
  },

  timeTextBackgroundStyles: {
    backgroundColor: Colors.textColor,
    borderRadius: 10,
    margin: 1
  },

  TitleStyle: {
    fontWeight: 'bold',
    fontSize: 18,
    left: 10,
    top: 5
  },

  searchIconStyles: {
    width: 25,
    height: 25,
    left: 10
  },

  inputStyles: {
    left: 10
  },

  barcodeIconStyles: {
    width: 25,
    height: 25,
  },

  likeeIconStyles: {
    width: 25,
    height: 25,
  },

  item: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.secondary_color,
    borderColor: Colors.secondary_color,
    width: '90%',
  },

  MainTitleStyle: {
    fontWeight: 'bold',
    fontSize: 22,
    left: 10,
    top: 10,
    color: 'white'
  },

  PriceStyle: {
    fontWeight: 'bold',
    fontSize: 25,
    left: 10,
    top: 10
  },
  ratingContainerStyles: {
    flexDirection: 'row',
  },
  ratingTextStyle: {
    fontSize: 18,
    left: 10,
    top: 5,
    color: 'gray'
  },
  gridView: {
    marginTop: 20,

  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 10,
    elevation: 6,
    width: 120,
    padding: 30,
    height: 120,
    margin: 10,
  },
  dealsContainerStyles: {
    borderRadius: 10,
    elevation: 3,
    width: 300,
    height: 200,
    margin: 10,
  },
  FlatListContainerStyles: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  itemName: {
    fontSize: 12,
    color: 'black',
    fontWeight: '600',
  },
  listSubTitleStyles: {
    fontSize: 14,
    left: 10,
    color: 'gray',
    fontWeight: '600',
  },
  ratingTextStyles: {
    fontSize: 14,
    paddingVertical: 5,
    left: 10,
    color: 'gray',
    fontWeight: '600',
  },
  listTitleStyle: {
    fontSize: 15,
    left: 10,
    color: 'black',
    fontWeight: '600',
  },
  timeTextStyles: {
    fontSize: 12,
    paddingVertical: 5,
    padding: 10,
    color: Colors.secondary_color
  },
  iconlImage: {
    width: 60,
    height: 55
  },
  coverImageStyles: {
    width: '100%',
    height: 120,
    borderRadius: 10,
    overflow: "hidden",
  },
  itemCode: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'black',
  },
});


export default styles;