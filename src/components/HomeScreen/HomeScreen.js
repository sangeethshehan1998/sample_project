
import React, { Component } from 'react';
import {
    View, Text, FlatList, Image, ImageBackground, TouchableOpacity
} from 'react-native';
import { Item, Input } from 'native-base'
import { Rating } from 'react-native-ratings';
import styles from './HomeScreenStyles'
import Colors from '../../resources/Colors';
import { ScrollView } from 'react-native-gesture-handler';
import { Categories, dealsOne, dealsTwo } from '../../storge/Storage'
export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rating: '',
            name: "KASUN"
        }
    }





    //counting rating
    ratingCompleted(rating) {

    }



    render() {
        return (

            <ScrollView showsVerticalScrollIndicator={false}>
                <ImageBackground source={require("../../assets/backgroudCover2.jpg")}
                    style={styles.backgroundCoverStyles}>
                    <View style={styles.subViewStyles}>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.searchIconStyles}
                                source={require('../../assets/search.png')}
                            />

                            <Input
                                style={styles.inputStyles}
                                placeholder="How Can I Help You?"
                                placeholderTextColor="gray"
                                autoCapitalize="none"

                            />

                            <TouchableOpacity >
                                <View style={styles.barcodeButtonStyles}>
                                    <View style={styles.barcodeIconContainerStyles}>
                                        <Image
                                            style={styles.barcodeIconStyles}
                                            source={require('../../assets/barcode.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableOpacity>

                        </Item>
                        <Text style={styles.MainTitleStyle}>HI {this.state.name},</Text>
                    </View>
                </ImageBackground>

                <View style={styles.subContainerSTyles}>
                    <View>
                        <View style={styles.subTitleContainerStyles}>
                            <Text style={styles.TitleStyle}>Categories</Text>

                            <TouchableOpacity>
                                <Image
                                    style={styles.barcodeIconStyles}
                                    source={require('../../assets/menu.png')}
                                />
                            </TouchableOpacity>
                        </View>

                        <FlatList
                            itemDimension={130}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            data={Categories}
                            horizontal={true}
                            style={styles.gridView}
                            renderItem={({ item, index }) => (
                                <View style={[styles.itemContainer, { backgroundColor: Colors.secondary_color }]}>
                                    <View style={styles.FlatListContainerStyles}>
                                        <Image
                                            name={item.name}
                                            style={styles.iconlImage}
                                            source={item.name}
                                        />
                                        <Text style={styles.itemName}>{item.title}</Text>
                                    </View>
                                </View>
                            )}
                        />
                        <View style={styles.subTitleContainerStyles}>
                            <Text style={styles.TitleStyle}>Deals of the Day</Text>

                            <Image
                                style={styles.barcodeIconStyles}
                                source={require('../../assets/menu.png')}
                            />

                        </View>

                        <FlatList
                            itemDimension={130}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            data={dealsOne}
                            horizontal={true}
                            style={styles.gridView}
                            renderItem={({ item, index }) => (
                                <View style={[styles.dealsContainerStyles, { backgroundColor: Colors.secondary_color }]}>

                                    <ImageBackground
                                        name={item.name}
                                        style={styles.coverImageStyles}
                                        source={item.name}
                                    >
                                        <TouchableOpacity>
                                            <View style={styles.likeIconContainerStyles}>
                                                <Image
                                                    style={styles.barcodeIconStyles}
                                                    source={require('../../assets/like.png')}
                                                />
                                            </View>
                                        </TouchableOpacity>
                                    </ImageBackground>
                                    <Text style={styles.listTitleStyle}>{item.title}</Text>
                                    <Text style={styles.listSubTitleStyles}>{item.subTitle}</Text>
                                    <View style={styles.timingContainerStyles}>
                                        <View style={styles.ratingSubContainerStyles}>
                                            <Rating
                                                imageSize={15}
                                                onFinishRating={this.ratingCompleted}
                                                style={{ paddingVertical: 5 }}
                                            />
                                            <Text style={styles.ratingTextStyles}>4.0</Text>
                                        </View>
                                        <View style={styles.timingSubContainerStyles}>
                                            <View style={styles.timeTextBackgroundStyles}>
                                                <Text style={styles.timeTextStyles}>250m</Text>
                                            </View>
                                            <View style={styles.timeTextBackgroundStyles}>
                                                <Text style={styles.timeTextStyles}>6-10min</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            )}
                        />
                        <View style={styles.subTitleContainerStyles}>
                            <Text style={styles.TitleStyle}>Explore More</Text>

                            <Image
                                style={styles.barcodeIconStyles}
                                source={require('../../assets/menu.png')}
                            />

                        </View>
                        <FlatList
                            itemDimension={130}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            data={dealsTwo}
                            horizontal={true}
                            style={styles.gridView}
                            renderItem={({ item, index }) => (
                                <View style={[styles.dealsContainerStyles, { backgroundColor: Colors.secondary_color }]}>

                                    <ImageBackground
                                        name={item.name}
                                        style={styles.coverImageStyles}
                                        source={item.name}
                                    >

                                    </ImageBackground>
                                    <Text style={styles.listTitleStyle}>{item.title}</Text>
                                    <Text style={styles.listSubTitleStyles}>{item.subTitle}</Text>
                                    <View style={styles.timingContainerStyles}>
                                        <View style={styles.ratingSubContainerStyles}>
                                            <Rating
                                                imageSize={15}
                                                onFinishRating={this.ratingCompleted}
                                                style={{ paddingVertical: 5 }}
                                            />
                                            <Text style={styles.ratingTextStyles}>4.0</Text>
                                        </View>
                                        <View style={styles.timingSubContainerStyles}>
                                            <View style={styles.timeTextBackgroundStyles}>
                                                <Text style={styles.timeTextStyles}>250m</Text>
                                            </View>
                                            <View style={styles.timeTextBackgroundStyles}>
                                                <Text style={styles.timeTextStyles}>6-10min</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            )}
                        />

                    </View>


                </View>
            </ScrollView >

        )
    }
}

