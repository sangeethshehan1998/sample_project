import {
    PixelRatio,
    StyleSheet,
  } from 'react-native';
  import Colors from '../../resources/Colors'
  
  const styles = StyleSheet.create({
    tabsBackground:{
        backgroundColor: Colors.primaryColor
    }
  });
  
  
  export default styles;