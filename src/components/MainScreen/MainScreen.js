import React, { Component } from 'react';
import {  Icon,  Container, Header, Tabs, Tab, TabHeading ,Drawer} from 'native-base'
import Colors from '../../resources/Colors';
import Home from '../HomeScreen/HomeScreen';
import styles from './MainScreenStyles'

export default class MainScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            
        }
    }




    render() {
        return (
            
            <Container>
                <Tabs tabBarPosition={'bottom'} initialPage={1} locked>
                    <Tab heading={<TabHeading style={styles.tabsBackground}><Icon name="heart" /></TabHeading>} >
                        <Home></Home>
                    </Tab>
                    <Tab heading={<TabHeading style={styles.tabsBackground}><Icon name="home" /></TabHeading>} >
                        <Home></Home>
                    </Tab>
                    <Tab heading={<TabHeading style={styles.tabsBackground}><Icon name="person" /></TabHeading>}>
                        <Home></Home>
                    </Tab>
                </Tabs>
            </Container>
            
        )
    }
}