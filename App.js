/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { StatusBar, View, } from 'react-native'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, Header } from 'react-navigation-stack';


//Screens
import HomeScreen from './src/components/HomeScreen/HomeScreen';
import MainScreen from './src/components/MainScreen/MainScreen';


const RootStack = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: { header: null }
  },
  MainScreen: {
    screen: MainScreen,
    navigationOptions: { header: null }
  },
},

  {
    initialRouteName: 'MainScreen'
  },
  {
    headerMode: 'screen'
  },
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar barStyle="light-content" hidden={false} backgroundColor="#3e307f" />
        <AppContainer />
      </View>
    );
  }
}




